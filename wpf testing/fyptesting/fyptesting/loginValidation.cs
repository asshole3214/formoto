﻿
using System;
using System.Windows;
using System.IO;
using System.Data.OleDb;
using System.Data;
namespace fyptesting
{
    class loginValidation
    {
        private string username { get; set; }// object username 
        private string password { get; set; } // object password 
        public bool state { get; set; } // whether the person credentials is correct or not 
        private string errormessage() // method for error handling 
        {
            return "Please enter a valid input";
        }
        public loginValidation() // defualt case 
        {
            username = "";
            password = "";
        }

        public void Initialize(string user, string pass) // method for defualt data source 
        {

            username = user;// settinng string username in gobal variable
            password = pass;// settinng string passowrd in gobal variable
            if (username == "" || password == "")// error handling 
            {
                MessageBox.Show(errormessage());// calling method in errormessage(); 
                return; 
            }
            state = referencefile(username, password, "userdatafile.txt");
            if (state == false) { MessageBox.Show(errormessage()); }

        }
        public bool Initialize(string user, string pass, string reference) // method for initialising if you want to refernce another data source 
        {

            username = user;// settinng string username in gobal variable
            password = pass;// settinng string passowrd in gobal variable
            string methodtype;
            if (username == "" || password == "")// error handling 
            {
                MessageBox.Show(errormessage());// calling method in errormessage(); 
                return false ;
            }
            try
            {
                methodtype = "methodtype1"; // indication is which method type
                state = referencedatabase(username, password, reference); //calling refence file type
                if (state) // checking if connection is successful
                {


                   
                    return true; 

                }
                else
                {
                    MessageBox.Show("login validation is invalid ");
                    return false; 
                }
            }
            catch (Exception e)
            {
                methodtype = "methodtype2";
                MessageBox.Show(e.Message+ "" +methodtype );
                return false;
            }
        }
        private bool referencefile(string user, string pass, string source) // method for referencing outside data for the notepad 
                                                                            // this is working for txt file only (for now will update ltr ) 
        {
            StreamReader read = new StreamReader(source); // references the text file 
            string line = "";

            while (line != null) // checking if line is empty or not 
            {
                line = read.ReadLine(); // reading the line 
                string[] userdata = line.Split('\t'); // differeiate  the different datas 
                if (userdata[1] == user && userdata[2] == pass) // ensuring the data is valid
                {
                    return true;
                }
                if (read.EndOfStream) // if there is no match data 
                {
                    return false;
                }
            }
            read.Close();
            return false; // default return type 
        }

        private bool referencedatabase(string user, string pass, string source) // method to refence data from database 
        {

            OleDbConnection conntodb = new OleDbConnection(source);
            conntodb.Open();
            string query = "SELECT [student table].[Admin no] FROM [student table] WHERE [student table].[Admin no] = '" + user + "' AND [student table].password = '" + pass + "';";
            OleDbCommand cmd = new OleDbCommand(query, conntodb);
            OleDbDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
               
                return true;
            }
            else
            {
                MessageBox.Show("Wrong Login credintials ");
                return false;
            }
        }
       
    }
}
