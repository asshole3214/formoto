#include <Servo.h>
const int powerpinsm = 13;
const int Up = 2;// delare as pin 2 for up
const int Down = 5;// declare as pin 5 for down
const int sigsm = 7 ;
int buttonStateUp = 0;
int buttonStateDown = 0;
Servo Reclin;
int position = 0;

void setup() {
  Serial.begin(9600);
  Reclin.attach(sigsm);
  pinMode (powerpinsm, OUTPUT);
  pinMode(Up, INPUT);//declare up as input
  pinMode(Down, INPUT);//declare down as input
  //digitalWrite (powerpinsm, LOW);
}


void loop() {
  buttonStateUp = digitalRead(Up);
  buttonStateDown = digitalRead(Down);
  digitalWrite(powerpinsm,HIGH);
  if (buttonStateDown == HIGH && buttonStateUp == HIGH)
  {
    Serial.write("null\n");
    digitalWrite(powerpinsm, LOW);
    Reclin.write(0);
    delay(5);
  }
  else if (buttonStateUp == HIGH && position < 360)
  {

    Serial.write("up\n");
    digitalWrite(powerpinsm, HIGH);
    Reclin.write(position++);
    delay(5);
  }
  else if (buttonStateDown == HIGH && position > 3)
  {
    Serial.write("down\n");
    digitalWrite(powerpinsm, HIGH);
    Reclin.write(position--);
    delay(5);
  }

  else
  {
    digitalWrite(powerpinsm, LOW);
  }
}

