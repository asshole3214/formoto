#include <Servo.h>

Servo reclin; 
const int buttonup =8;
const int buttondown = 9;
const int delaytime= 100;
const int sigpinsm = 7;
const int powerpinsm = 6;
int buttonupstate = 0, buttondownstate = 0 ; 

void setup() {
  Serial.begin(9600);
  pinMode(buttonup, INPUT); // decalring input for up button
  pinMode(buttondown,INPUT); // declaring input for down button
  pinMode(powerpinsm,OUTPUT); // declaring output for power of servo motor 
  reclin.attach(sigpinsm); // declaring pin for servo motor pin 

}

void loop() {
  
  buttonupstate = digitalRead(buttonup); // reading state of button up input
  buttondownstate = digitalRead(buttondown); //  reading state of button up input
  
  if (buttondownstate==1 && buttonupstate == 1)//event when both button is pressed
  {
    Serial.write("null\n");
    reclin.write(0);
  }
  else if (buttonupstate == 1  ) // event when button up is pressed 
  {
    // try this new method first
    Serial.write("up\n");
    digitalWrite(powerpinsm,HIGH) ; // giving power to servo motor 
     /* buttonupstate = digitalRead(buttonup);
      if (buttonupstate == 1){break;}*/ 
    reclin.write(180); // setting to 180 degree 
    delay(delaytime);
  }

  else if (buttondownstate==1)
  {
    Serial.write("down\n");
   /* for (int i =180 ;i>0 ; i-=10 )
    {
      buttondownstate = digitalRead(buttondown);
      if (buttondownstate == 1){break;}*/
    digitalWrite(powerpinsm,HIGH) ; // giving power to servo motor 
    reclin.write(0); // setting servo motor to 0 degree
    delay(delaytime);
    //}
  }

 /* else 
   {
    //Serial.write("no input\n");
    reclin.write(0);
   }*/
 
}
